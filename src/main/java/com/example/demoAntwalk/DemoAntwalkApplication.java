package com.example.demoAntwalk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoAntwalkApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoAntwalkApplication.class, args);
	}

}
