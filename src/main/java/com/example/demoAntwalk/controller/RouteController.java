package com.example.demoAntwalk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoAntwalk.service.RouteService;
import com.example.demoAntwalk.model.Route;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



@RestController
@RequestMapping("/route")
public class RouteController {
    
    @Autowired
    private RouteService routeService;

    @GetMapping(value="/getAll")
    public List<Route> getAllRoutes() {
        return routeService.getAllRoutes();
    }

    @GetMapping(value="/get/{id}")
    public Route getRouteById(@PathVariable int id) {
        return routeService.getById(id);
    }

    @PostMapping(value="/update")
    public Route updateRoute(@RequestBody Route route) {    
        routeService.updateRoute(route);
        return route;
    }

    @PostMapping(value="/add")
    public Route addRoute(@RequestBody Route route) {
        routeService.addRoute(route);
        return route;
    }

    @DeleteMapping(value="/delete/{id}")
    public String deleteRoute(@PathVariable int id){
        routeService.deleteRoute(id);
        return "Route with id "+id+" has been deleted";
    }
    
    
    
    
}
