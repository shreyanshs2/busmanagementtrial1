package com.example.demoAntwalk.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoAntwalk.model.Product;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;




@RestController
public class MainController {

    List<Product> productList=new ArrayList();	
	public MainController() {
		super();
		productList.add(new Product(1001,"TV",95000,1,"Apple",0));
		productList.add(new Product(1002,"Tripod",7000,2,"Digitech",100));
		productList.add(new Product(1003,"CCTV Camera",4500,5,"Mi",150));
		productList.add(new Product(1004,"Mobile",50000,1,"Samsung",0));
		productList.add(new Product(1005,"Monitor",16000,4,"Dell",100));
		productList.add(new Product(1006,"Mouse",1200,15,"logitech",50));
	}
    
    @GetMapping("/show")
    public String show(){
        return "hello World";
    }

    // get all products
    @GetMapping(value="/getAll")
    public List<Product> productList() {
        return productList;
    }

    // get 1 product
    @GetMapping(value="/get/{id}")
    public Product getMethodName(@PathVariable int id) {
        for(Product product: productList){
            if(product.prodId==id){
                return product;
            }
        }
        return null;
    }

    // update product
    @PutMapping(value="updateProduct/{id}")
    public void putMethodName(@PathVariable int id, @RequestParam String field, @RequestParam String value) {
        for(Product product: productList)        {
            if(product.getProdId()==id){
                if(field.equalsIgnoreCase("price")){
                    product.setProdId(Integer.parseInt(value));
                }
                else if(field.equalsIgnoreCase("name")){
                    product.setName(value);
                }
                else if(field.equalsIgnoreCase("")){
                    product.setName(value);
                }
                
                
            }
        }
    }
    
    // add product
    @PostMapping(value="addProduct")
    public Product addProduct(@RequestBody Product product) {
        productList.add(product);
        return product;
    }
    

    // delete product
    @DeleteMapping("/delete/{id}")
    public String deleteProduct(@PathVariable int id){
        for(Product product: productList){
            if(product.getProdId()==id){
                productList.remove(product);
                return "Product deleted";
            }
        }
        return "error";
    }    
    
}
