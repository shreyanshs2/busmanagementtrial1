package com.example.demoAntwalk.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoAntwalk.model.Employee;
import com.example.demoAntwalk.service.EmployeeService;




@RestController
@RequestMapping("/employee")
public class EmployeeController {
    
    @Autowired
    private EmployeeService employeeService;
	
    
    // get all employees
    @GetMapping(value="/getAll")
    public List<Employee> employeeList() {
        return employeeService.getAllEmployees();
    }

    // get 1 employee
    @GetMapping(value="/get/{id}")
    public Employee getEmployeeById(@PathVariable int id) {
        return employeeService.getEmployeeById(id);
    }

    // update employee
    @PostMapping(value="/updateEmployee")
    public void putMethodName(@RequestBody Employee employee) {
        System.out.println("Deleting "+employee);
        employeeService.updateEmployee(employee);

    }
    
    // add employee
    @PostMapping(value="/addEmployee")
    public Employee addEmployee(@RequestBody Employee employee) {
        employeeService.addEmployee(employee);
        System.out.println("The employee is "+employee.toString());
        return employee;
    }
    

    // delete employee
    @DeleteMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable int id){
        employeeService.deleteEmployeeById(id);
        return "error";
    }    
    
}

