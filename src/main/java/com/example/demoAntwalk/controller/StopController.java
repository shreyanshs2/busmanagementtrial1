package com.example.demoAntwalk.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoAntwalk.model.Stop;
import com.example.demoAntwalk.service.StopService;

@RestController
@RequestMapping("/stop")
public class StopController {
    
    @Autowired
    private StopService stopService;

    @GetMapping(value="/getAll")
    public List<Stop> getAllstops() {
        return stopService.getAllStops();
    }

    @GetMapping(value="/get/{id}")
    public Stop getStopById(@PathVariable int id) {
        return stopService.getById(id);
    }

    @PostMapping(value="/update")
    public Stop updateStop(@RequestBody Stop stop) {    
        stopService.updateStop(stop);
        return stop;
    }

    @PostMapping(value="/add")
    public Stop addStop(@RequestBody Stop stop) {
        stopService.addStop(stop);
        return stop;
    }

    @DeleteMapping(value="/delete/{id}")
    public String deleteStop(@PathVariable int id){
        stopService.deleteStop(id);
        return "stop with id "+id+" has been deleted";
    }
}
