package com.example.demoAntwalk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demoAntwalk.model.Route;
import com.example.demoAntwalk.repository.RouteRepository;

@Service
public class RouteService {
    
    @Autowired
    private RouteRepository routeRepository;

    public List<Route> getAllRoutes(){
        return routeRepository.findAll();
    }

    public Route getById(int id) {
        return routeRepository.findById(id).get();
    }

    public void updateRoute(Route route) {
        routeRepository.save(route);
    }

    public void addRoute(Route route) {
        routeRepository.save(route);
    }

    public void deleteRoute(int id) {
        routeRepository.deleteById(id);
    }

}
