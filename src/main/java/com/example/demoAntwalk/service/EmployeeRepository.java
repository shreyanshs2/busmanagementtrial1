package com.example.demoAntwalk.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demoAntwalk.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{
    // JpaRepo contains save(), find(), findAll()
    // Employee respresents entity, Integer is the column of primary key
}
