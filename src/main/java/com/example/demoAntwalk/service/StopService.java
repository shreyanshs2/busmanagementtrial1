package com.example.demoAntwalk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demoAntwalk.model.Stop;
import com.example.demoAntwalk.repository.StopRepository;

@Service
public class StopService {
    
    @Autowired
    private StopRepository stopRepository;

    public List<Stop> getAllStops() {
        return stopRepository.findAll();
    }

    public void updateStop(Stop stop) {
        stopRepository.save(stop);
    }
    
    public void addStop(Stop stop) {
        stopRepository.save(stop);        
    }

    public void deleteStop(int id) {
        stopRepository.deleteById(id);
    }

    public Stop getById(int id) {
        return stopRepository.findById(id).get();
    }
}
