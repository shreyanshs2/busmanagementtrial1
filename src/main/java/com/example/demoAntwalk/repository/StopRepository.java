package com.example.demoAntwalk.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demoAntwalk.model.Stop;

public interface StopRepository extends JpaRepository<Stop, Integer>{
    
}
