package com.example.demoAntwalk.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demoAntwalk.model.Route;


public interface RouteRepository extends JpaRepository<Route, Integer>{
    
}
