package com.example.demoAntwalk.model;

import java.sql.Time;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "stop")
public class Stop {

    @Id
    @Column(name = "stop_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "stop_name")
    private String name;

    @ManyToMany(mappedBy = "stops")
    private Set<Route> stops;
    
    public Stop() {
    }

    public Stop(Long id, String name, Time arrivalTime) {
        this.id = id;
        this.name = name;
        this.arrivalTime = arrivalTime;
    }

    @Column(name = "arrival_time")
    private Time arrivalTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Time getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Time arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @Override
    public String toString() {
        return "Stop [id=" + id + ", name=" + name + ", arrivalTime=" + arrivalTime + "]";
    }
}
