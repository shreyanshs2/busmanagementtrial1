package com.example.demoAntwalk.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "route")
public class Route{

    @Id
    @Column(name = "route_id")
    private int id;

    // @Id
    @Column(name = "route_start")
    private String start;

    @Column(name = "route_end")
    private String end;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "routes_stops",
        joinColumns = @JoinColumn(name = "route_id"),
        inverseJoinColumns = @JoinColumn(name = "stop_id")
    )
    @Column(name = "route_stops")
    private List<Stop> stops;


    public Route() {
    }

    public Route(int id, String start, String end, List<Stop> stops) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.stops = stops;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "Route [id=" + id + ", start=" + start + ", end=" + end + ", stops=" + "]";
    }

}
